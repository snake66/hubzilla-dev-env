# A docker-compose based dev env for Hubzilla

Assumes the Hubzilla core directory is in `../core`.

Creates three containers:

- db: The database server (postgres)
- hubzilla: The front end nginx reverse porxy
- hubzilla-php-fpm: The back end php server.

It's rough, but seems to work relatively well for a local dev instance that
don't need to communicate with the wited fediverse for now.

Run with `docker-compose up`

Add an alias for localhost in your hosts file, like this:

```
127.0.0.1    localhost  hubzilla
```

Connect to `http://hubzilla:3000`.
